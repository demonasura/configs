## Directory Structure:

    .
    ├── ~  
    │   ├── .face.png  
    │   ├── Pictures  
    │               ├── Wallpapers-Linux  
    │                                   ├── *.*  
    └── ...

&

    .
    ├── /  
    │   ├── boot  
    │           ├── grub  
    │                   ├── grub.jpg  
    └── ...
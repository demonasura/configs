#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '
eval "$(starship init bash)"
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'
alias update='paru -Sy'
alias upgrade='paru -Syyu'
alias upd='paru -Sy'
alias upg='paru -Syyu'
clean=~/.config/clean.sh
boot=~/.config/boot.sh

# ~/.config/fish/config.fish

if status is-interactive
    # Commands to run in interactive sessions can go here
end
alias rm="rm -i"
alias mv="mv -i"
alias cp="cp -i"
alias update="paru -Sy"
alias upgrade="paru -Syyu"
alias upd="paru -Sy"
alias upg="paru -Syyu"
